const os = require("os");
const fs = require("fs");
const data = require("./src/data.json");
// const segitiga = require("./src/mymodule/segitiga");

const kubus = require("./src/mymodule/kubus");
const limas = require("./src/mymodule/limas");
const lingkaran = require("./src/mymodule/lingkaran");

// for edit by id
kubus.edit({id:93 , payload: {value:30, type: "luas", jenis:"kubus" }});
// delete using splice id 1
kubus.splice(94);
// limas.edit({id:95 , payload: {value:25, type: "volume", jenis:"limas segitiga" }});
// kubus.splice(96);
// lingkaran.edit({id:101 , payload: {value:12, type: "volume", jenis:"lingkaran" }});
// kubus.splice(102);

kubus.volumekubus(5,5,5);
kubus.volumekubus(3,2,1);
kubus.volumekubus(2,1,2);
kubus.luaskubus(4);
kubus.luaskubus(10);
kubus.luaskubus(12);

limas.volumelimas(4,2);
limas.volumelimas(1,2);
limas.volumelimas(5,4);
limas.luaslimas(1,2);
limas.luaslimas(2,2);
limas.luaslimas(3,4);


lingkaran.volumelingkaran(1);
lingkaran.volumelingkaran(2);
lingkaran.volumelingkaran(3);
lingkaran.luaslingkaran(3);
lingkaran.luaslingkaran(2);
lingkaran.luaslingkaran(1);