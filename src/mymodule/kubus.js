const fs = require("fs");
const data = require("../data.json");

function volumekubus(sisi){
    rumus = sisi*sisi*sisi;
    console.log(`jumlah volume kubus :${rumus} `);
    saved(rumus, "volume", "kubus");
}
function luaskubus(sisi){
    rumus = 6*sisi;
    console.log(`jumlah luas kubus :${rumus} `);
    saved(rumus, "luas", "kubus");
}
function splice(id){
    const index = data.findIndex((row) =>row.id ===id);
    data.splice (index,1);
    fs.writeFile("./src/data.json", JSON.stringify(data), "utf-8", (err) => {
        if(err) throw err;
        console.log("delete");
    });
    console.log(data);
}    
function edit (param){
    const {id, payload} = param;
    let newData = data;
    const index = newData.findIndex((row) =>row.id ===id);
    newData[index] ={
        ...newData[index],
        ...payload,
        
    }
    console.log(newData);
};
function saved(value,type,jenis){
    let kriteria = {
        id: data.length,
        value,
        type,
        jenis,
    };
    let newData = data;
    newData.push(kriteria);
    console.log(newData);
    fs.writeFile("./src/data.json", JSON.stringify(newData), "utf-8", (err) => {
        if(err) throw err;
        console.log("Saving Data");
    });
}
module.exports = {volumekubus,luaskubus,edit,splice};