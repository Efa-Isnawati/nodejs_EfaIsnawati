const fs = require("fs");
const data = require("../data.json");

//Limas Segitiga
function volumelimas(alas, tinggi){
    rumus = 1/3*(alas*tinggi);
    console.log(`jumlah volume limas segitiga :${rumus} `);
    saved(rumus, "volume", "limas segitiga");
}
function luaslimas(a,t){
    // 1/2 x a x t
    rumus = 1/2*(a*t);
    console.log(`jumlah luas limas segitiga :${rumus} `);
    saved(rumus, "luas", "limas segitiga");
}
function splice(id){
    const index = data.findIndex((row) =>row.id ===id);
    data.splice (index,1);
    fs.writeFile("./src/data.json", JSON.stringify(data), "utf-8", (err) => {
        if(err) throw err;
        console.log("delete");
    });
    console.log(data);
}    
function edit (param){
    const {id, payload} = param;
    let newData = data;
    const index = newData.findIndex((row) =>row.id ===id);
    newData[index] ={
        ...newData[index],
        ...payload,
        
    }
    console.log(newData);
};
function saved(value,type,jenis){
    let kriteria = {
        id: data.length,
        value,
        type,
        jenis,
    };
    let newData = data;
    newData.push(kriteria);
    console.log(newData);
    fs.writeFile("./src/data.json", JSON.stringify(newData), "utf-8", (err) => {
        if(err) throw err;
        console.log("Saving Data");
    });
}
module.exports = { volumelimas,luaslimas,edit,splice};