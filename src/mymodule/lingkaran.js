const fs = require("fs");
const data = require("../data.json");
// V = 4/3 x π x r^3
// L = 4 × π × r²
function volumelingkaran(rjarijari){
    rumus = 4/3*Math.PI*Math.pow(rjarijari,3);
    console.log(`jumlah volume lingkaran :${rumus} `);
    saved(rumus, "volume", "lingkaran");
}
function luaslingkaran(rjarijari){
    rumus = 4/3*Math.PI*Math.pow(rjarijari,2);
    console.log(`jumlah luas lingkaran :${rumus} `);
    saved(rumus, "luas", "lingkaran");
}
function splice(id){
    const index = data.findIndex((row) =>row.id ===id);
    data.splice (index,1);
    fs.writeFile("./src/data.json", JSON.stringify(data), "utf-8", (err) => {
        if(err) throw err;
        console.log("delete");
    });
    console.log(data);
}    
function edit (param){
    const {id, payload} = param;
    let newData = data;
    const index = newData.findIndex((row) =>row.id ===id);
    newData[index] ={
        ...newData[index],
        ...payload,
        
    }
    console.log(newData);
};
function saved(value,type,jenis){
    let kriteria = {
        id: data.length,
        value,
        type,
        jenis,
    };
    let newData = data;
    newData.push(kriteria);
    console.log(newData);
    fs.writeFile("./src/data.json", JSON.stringify(newData), "utf-8", (err) => {
        if(err) throw err;
        console.log("Saving Data");
    });
}
module.exports = {volumelingkaran,luaslingkaran,edit,splice};